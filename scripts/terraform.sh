#!/usr/bin/env bash

### Install / update terraform and terragrunt

set -euo pipefail

TF_VERSION="${TF_VERSION:-1.5.7}"
TG_VERSION="${TG_VERSION:-0.53.6}"

install_terraform() {
    if terraform version | grep "Terraform v${TF_VERSION}" >/dev/null; then
        echo "terraform $TF_VERSION already installed."
        return
    fi

    echo "Downloading terraform $TF_VERSION ..."
    cd /tmp
    wget -q "https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_$(dpkg --print-architecture).zip"
    unzip "terraform_*.zip"
    sudo mv terraform /usr/local/bin/
    rm -rf terraform*
    cd -
}

install_terragrunt() {
    if terragrunt --version | grep "terragrunt version v${TG_VERSION}" >/dev/null; then
        echo "terragrunt $TG_VERSION already installed."
        return
    fi

    echo "Downloading terragrunt $TG_VERSION ..."
    cd /tmp
    wget -q "https://github.com/gruntwork-io/terragrunt/releases/download/v${TG_VERSION}/terragrunt_linux_$(dpkg --print-architecture)" -O terragrunt
    sudo install -o root -g root -m 0755 terragrunt /usr/local/bin/terragrunt
    rm -f ./terragrunt
    cd -
}

install_terraform
install_terragrunt
