#!/usr/bin/env bash

### Configure networkd

set -euo pipefail

ROOT_DIR="${ROOT_DIR:-$(git rev-parse --show-toplevel)}"

configure_networkd() {
    for src in "$ROOT_DIR/configs/"*.network; do
        netwk=$(basename "$src")
        dest="/etc/systemd/network/10-netplan-${netwk}"
        if cmp -s "$src" "$dest"; then
            echo "${netwk} config up to date."
            continue
        fi
        echo "Applying $dest"
        sudo cp -fa "$src" "$dest"
    done
    sudo networkctl reload
}

configure_networkd

printf "VM IPs:"
hostname -I
