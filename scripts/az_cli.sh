#!/usr/bin/env bash

### Install / update the az cli
set -euo pipefail

apt_setup() {
    # https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt
    keyring="/etc/apt/keyrings/microsoft.gpg"
    if [ ! -s "$keyring" ]; then
        echo "Downloading microsoft gpg key to $keyring"
        curl -fsLS https://packages.microsoft.com/keys/microsoft.asc |sudo gpg --dearmor -o "$keyring"
        sudo chmod a+r "$keyring"
    fi

    apt_list="/etc/apt/sources.list.d/azure-cli.list"
    if [ ! -s "$apt_list" ]; then
        echo "Adding azure-cli to apt source list ($apt_list)"
        echo \
        "deb [arch=$(dpkg --print-architecture) signed-by=$keyring] https://packages.microsoft.com/repos/azure-cli/ \
        $(lsb_release -cs) main" | sudo tee "$apt_list"
    fi
}

apt_setup

sudo apt-get update -qq
sudo apt-get install -yq azure-cli
