#!/usr/bin/env bash

### Copy any ssh keys, and config, in host's .ssh dir to VM's .ssh dir
### Only overriding if newer (and backing up whenever replaced)

set -euo pipefail

ssh_dir_guess=$(echo /mnt/host/*/.ssh/)
SSH_DIR="${SSH_DIR:-$ssh_dir_guess}"
if ! [ -d "$SSH_DIR" ]; then
    printf "\nERROR: %s not found.\n" "$SSH_DIR" >&2
    printf " Please set 'SSH_DIR' to correct location, and re-run.\n" >&2
    exit 1
fi

shopt -s nullglob
mkdir -p "${HOME}/.ssh"
echo "Synchronizing SSH keys and config from $SSH_DIR"
for key in "$SSH_DIR"/id_*; do
    basename "$key"
    cp -ua --backup=numbered "$key" "${HOME}/.ssh/"
done
# ssh config as well
cp -ua --backup=numbered "$SSH_DIR"/config "${HOME}/.ssh/"
