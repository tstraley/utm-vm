#!/usr/bin/env bash

### Install, update, and (re)configure docker
set -euo pipefail

ROOT_DIR="${ROOT_DIR:-$(git rev-parse --show-toplevel)}"

apt_setup() {
    # https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
    keyring="/etc/apt/keyrings/docker.gpg"
    if [ ! -s "$keyring" ]; then
        echo "Downloading docker gpg key to $keyring"
        curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o "$keyring"
        sudo chmod a+r "$keyring"
    fi

    apt_list="/etc/apt/sources.list.d/docker.list"
    if [ ! -s "$apt_list" ]; then
        echo "Adding docker to apt source list ($apt_list)"
        echo \
        "deb [arch=$(dpkg --print-architecture) signed-by=$keyring] https://download.docker.com/linux/ubuntu \
        $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | sudo tee "$apt_list"
    fi
}

configure_daemon() {
    src="$ROOT_DIR/configs/docker_daemon.json"
    if [ -f "$src" ]; then
        dest="/etc/docker/daemon.json"
        if cmp -s "$src" "$dest"; then
            echo "Docker daemon config up to date."
            return
        fi
        echo "Applying $dest"
        dockerd --validate --config-file "$src"
        sudo cp -fa "$src" "$dest"
        sudo systemctl restart docker
    fi
}

apt_setup

sudo apt-get update -qq
sudo apt-get install -yq docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

if ! groups | grep "docker" >/dev/null; then
    echo "Adding user ($(whoami)) to docker group"
    sudo usermod -aG docker "$USER"
fi

configure_daemon
