#!/usr/bin/env bash

### Install Go
set -euo pipefail

if command -v go >/dev/null; then
    echo "go already installed, not upgrading"
    exit 0
fi

VERS=$(curl -fsS https://go.dev/VERSION?m=text | head -n1)
echo "Downloading Go $VERS ..."
cd /tmp
wget -q "https://dl.google.com/go/$VERS.linux-$(dpkg --print-architecture).tar.gz" -O golang.tar.gz
sudo tar -C /usr/local -xf golang.tar.gz
rm ./golang.tar.gz
cd -

# mkdir -p "$GOPATH" "$GOPATH/src" "$GOPATH/pkg" "$GOPATH/bin"
