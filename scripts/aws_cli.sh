#!/usr/bin/env bash

### Install the aws cli
set -euo pipefail

ROOT_DIR="${ROOT_DIR:-$(git rev-parse --show-toplevel)}"

install() {
    if command -v aws >/dev/null; then
        echo "aws cli already installed, not upgrading"
        return
    fi

    cd /tmp
    echo "Downloading aws-cli package..."
    curl -fsS "https://awscli.amazonaws.com/awscli-exe-linux-$(arch).zip" -o "awscliv2.zip"
    unzip -q awscliv2.zip
    sudo ./aws/install

    rm -rf ./aws/
    rm -f awscliv2.zip
    cd -
}

configure() {
    src="$ROOT_DIR/configs/aws_config"
    if [ -f "$src" ]; then
        dest="${HOME}/.aws/config"
        mkdir -p "$(dirname "$dest")"
        if cmp -s "$src" "$dest"; then
            echo "AWS config up to date."
            return
        fi
        echo "Applying $dest"
        cp -fa "$src" "$dest"
    fi
}

install

if ! grep aws_completer ~/.bash_completion >/dev/null; then
    ## Configure bash completion
    echo "Adding aws-cli completion to ~/.bash_completion"
    echo "complete -C '/usr/local/bin/aws_completer' aws" >> ~/.bash_completion
fi

configure
