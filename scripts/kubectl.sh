#!/usr/bin/env bash

### Install / update kubectl
set -euo pipefail

CUR_VER=""
if command -v kubectl >/dev/null; then
    CUR_VER=$(kubectl version --client | grep "Client Version:" | cut -f3 -d' ')
    echo "kubectl $CUR_VER already installed. Attempting update..."
fi

VERS=$(curl -fSsL https://dl.k8s.io/release/stable.txt)
if [ "$VERS" == "$CUR_VER" ]; then
    echo "kubectl is already the latest version"
    exit 0
else
    echo "Downloading kubectl $VERS ..."
    cd /tmp
    curl -fSsLO "https://dl.k8s.io/release/$VERS/bin/linux/$(dpkg --print-architecture)/kubectl"
    sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
    rm -f ./kubectl
    cd -
fi

completion="/etc/bash_completion.d/kubectl"
echo "Applying kubectl bash completion to $completion"
kubectl completion bash | sudo tee "$completion" > /dev/null
sudo chmod a+r "$completion"
