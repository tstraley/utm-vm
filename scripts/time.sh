#!/usr/bin/env bash

### Configure systemd timesyncd with optional config
### Set timezone, as needed
set -euo pipefail

ROOT_DIR="${ROOT_DIR:-$(git rev-parse --show-toplevel)}"

configure_timesyncd() {
    local src dest
    src="$ROOT_DIR/configs/timesyncd.conf"
    if [ -f "$src" ]; then
        dest="/etc/systemd/timesyncd.conf"
        if cmp -s "$src" "$dest"; then
            echo "Timesyncd config up to date."
            return
        fi
        echo "Applying $dest"
        sudo cp -fa "$src" "$dest"
        sudo systemctl daemon-reload
        sudo systemctl restart systemd-timesyncd
    fi
}

enable_ntp() {
    ntp_on="$(timedatectl show -p NTP --value)"
    if [ "$ntp_on" != "yes" ] && [ "$ntp_on" != "true" ]; then
        echo "Enabling NTP"
        sudo timedatectl set-ntp true
    fi
}

set_timezone() {
    cur_tz=$(timedatectl show -p Timezone --value)
    echo "Timezone currently set to $cur_tz"
    TZ="${TZ:-${TIMEZONE:-}}"
    if [ -n "$TZ" ] && [ "$TZ" != "$cur_tz" ]; then
        echo "Setting timezone to $TZ"
        sudo timedatectl set-timezone "$TZ"
        return
    fi
    echo "If you'd like to change this, re-run this script with 'TIMEZONE' env var assigned."
    echo "To see a list of available timezones, run 'timedatectl list-timezones'."
}

configure_timesyncd
enable_ntp
set_timezone