#!/usr/bin/env bash

### Copy additional configuration files for initial provisioning
### These will only be copied over if they exist in repo configs
### and NOT in destination already (these will not overwrite
### locally modified config files on the VM).

set -euo pipefail

ROOT_DIR="${ROOT_DIR:-$(git rev-parse --show-toplevel)}"

copy_config() {
    local src dest
    src=$1
    dest=$2
    if [ ! -f "$src" ]; then
        return #noop
    fi
    if [ -e "$dest" ]; then
        echo "Config already exists at $dest"
        echo "Will not replace."
    fi

    echo "Applying $dest"
    sudo cp -fa "$src" "$dest"
}

# gitconfig
copy_config "$ROOT_DIR/configs/gitconfig" "${HOME}/.gitconfig"
# bash profile
copy_config "$ROOT_DIR/configs/bash_profile" "${HOME}/.bash_profile"
# netrc
copy_config "$ROOT_DIR/configs/netrc" "${HOME}/.netrc"
