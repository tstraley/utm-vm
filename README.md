# UTM VM

Config and scripts for UTM Linux VM running on Mac.

This project supersedes the Vagrant VMs project I wrote due to vagrant (and VirtualBox) 
not having proper support for the new ARM-based Apple Silicon.

Currently, must of the initial bootstrapping and VM setup is simply
documented below. In the future more of this can be automated (eg. the 
network and SSH setup should be script-able).

## Prerequisites

If starting on a fresh Mac, start with setting up your Mac command line interface:
1. Install the "command line developer tools" provided by Apple
2. Install Homebrew for being able to install other cli packages
    - https://docs.brew.sh/Installation

Install UTM: https://docs.getutm.app/installation/macos/
* Github download is free

## Set up Linux VM

### Download Linux ISO
I recommend Ubuntu Server arm64: https://ubuntu.com/download/server/arm

While amd64 can work on Mac with emulation, performance is worse, so this guide uses virtualization on same architecture.


### Create the new VM in UTM
UTM provides some guidance here: https://docs.getutm.app/guides/ubuntu/

I choose some different settings, though:
1. In UTM -> New VM -> Virtualize -> Linux
1. Select **Use Apple Virtualization**
    - the alternative, QEMU, doesn't support virtioFS storage sharing
1. Load the ISO
1. Select resources
    - these can be increased later, but 6 cores, 6GB RAM, and 64 GB disk should be plenty
1. Start -> Boot

### Install Linux into the VM
A UTM terminal connected to the VM's tty will walk through TUI install
1. You will mostly enter through the prompts with no changes
1. Set a username and password (remember these for later)
1. Consider changing the storage config
    - eg. increase root lv from available disk
1. Select **Install openssh**
1. After successful install: shutdown the VM and remove the ISO mount device
1. Start VM
    * It should now go to a login prompt, rather than install menu

### Mount your Mac Home directory

1. If not already configured, assign a "Shared Directory" in UTM
    for your VM, that maps to your home directory (eg. `/Users/t.straley`).
1. Log in to the VM via the UTM terminal login prompt
    - username and password were provided during install

Execute the following:
```shell
printf "\nshare /mnt/host virtiofs rw,nofail 0 0\n" | sudo tee -a /etc/fstab
sudo mkdir -p /mnt/host
sudo mount -a
ls -l /mnt/host/*  # confirm expected data is mounted and available
```

Optionally, create symlinks for easier usage (eg.
`sudo ln -s /mnt/host/t.straley/ /host_home`)


### Configure Networking

The default UTM VM will use DHCP assigned IP address. This gives us a static IP to use.

> Note: Apple virtualization provides shared network addresses from a specific subnet
controlled by the Mac host. This subnet is configured in
`/Library/Preferences/SystemConfiguration/com.apple.vmnet.plist`, with a default of
`192.168.64.1`. If yours happens to be (or needs to be) different, various configuration
in this repo needs to change to match.

1. In the VM terminal, get to the directory of this repo via the host mount
    - eg. `cd /mnt/host/t.straley/Projects/utm-vm`
1. Execute the networkd setup script
    - `./scripts/networkd.sh`

### Configure SSH

At this point you should be able to SSH to the VM using your
username and password, with the static IP address configured above:
```
$ ssh tim@192.168.64.64
```

This isn't ideal from a security standpoint, nor an automation and tooling
standpoint. So:

1. Create an ssh keypair
    - `ssh-keygen -t ed25519 -C "tim@stray-dev"`
    - Optionally, add a passphrase
    - Store this somewhere like `~/.ssh/id_ubuntu2204`
1. In the VM terminal, add the public key to the authorized keys file
    - `cat /mnt/host/t.straley/.ssh/id_ubuntu2204.pub >> ~/.ssh/authorized_keys`
1. Test it out
    - `ssh -i ~/.ssh/id_ubuntu2204 tim@192.168.64.64`
1. For improved security, edit `/etc/ssh/sshd_config` on the VM
    1. Set `Port 2222` to move off common default ssh port
    1. Set `ListenAddress 192.168.64.2` (or whatever static IP you used)
    1. Set `PasswordAuthentication no`
    1. `sudo systemctl restart sshd`
1. Create an entry in your host's `~/.ssh/config` for easy use
    - See [example in host configs dir](./configs/host/ssh.config)

Now a simple: `ssh <host>` (eg. `ssh ubuntu-22.04`) will connect you.

### Provisioning

A variety of scripts will manage configuration and installation of tools.
To provision everything, simply run `./provision.sh`

## Headless

When everything is stable, and the network / ssh configuration is working
reliably through reboots, you can make UTM and the VM run completely in the
background. To do so, edit the VM configuration while the guest is shut down,
and remove the "Display". (This is the UTM terminal that was used for initial
install and bootstrap access, but it subpar compared to using the Mac terminal
with SSH). Then, you can also adjust the UTM preferences to not show up in the
dock (it will only show in the menu bar).
