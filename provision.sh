#!/usr/bin/env bash

### Installs all basics and calls other provisioning scripts
set -euo pipefail

ROOT_DIR="$(cd "$(dirname "$0")" && pwd)"
export ROOT_DIR

install_basics() {
    echo "Gathering updates..."
	sudo apt-get update -yq
	sudo DEBIAN_FRONTEND=noninteractive apt-get install -yq \
		apt-transport-https \
		ca-certificates \
		curl \
		git \
		gnupg2 \
		jq \
		less \
		lsb-release \
        make \
		open-vm-tools \
		python3-pip \
        shellcheck \
		software-properties-common \
		unzip \
        vim \
		wget

	sudo apt-get clean
}

install_basics

echo -e "\n---------- time.sh -----------"
"$ROOT_DIR/scripts/time.sh"
echo -e "------------------------------\n"

echo -e "\n-------- networkd.sh ---------"
"$ROOT_DIR/scripts/networkd.sh"
echo -e "------------------------------\n"

echo -e "\n--------- configs.sh ---------"
"$ROOT_DIR/scripts/configs.sh"
echo -e "------------------------------\n"

echo -e "\n-------- ssh_copy.sh ---------"
"$ROOT_DIR/scripts/ssh_copy.sh"
echo -e "------------------------------\n"

echo -e "\n--------- docker.sh ----------"
"$ROOT_DIR/scripts/docker.sh"
echo -e "------------------------------\n"

echo -e "\n--------- az_cli.sh ----------"
"$ROOT_DIR/scripts/az_cli.sh"
echo -e "------------------------------\n"

echo -e "\n--------- aws_cli.sh ---------"
"$ROOT_DIR/scripts/aws_cli.sh"
echo -e "------------------------------\n"

echo -e "\n---------- golang.sh ---------"
"$ROOT_DIR/scripts/golang.sh"
echo -e "------------------------------\n"

echo -e "\n--------- kubectl.sh ---------"
"$ROOT_DIR/scripts/kubectl.sh"
echo -e "------------------------------\n"

echo -e "\n-------- terraform.sh --------"
"$ROOT_DIR/scripts/terraform.sh"
echo -e "------------------------------\n"
